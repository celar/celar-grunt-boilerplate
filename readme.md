# Celar Grunt Boilerplate
## NOTICE: This repository is 'outdated'. Please use: [https://bitbucket.org/celar/project-boilerplate/overview](https://bitbucket.org/celar/project-boilerplate/overview).

## Requirements

| Requirements                        | Versions |
| ----------------------------------- | -------- |
| [Sass](http://sass-lang.com)        | ~3.3.0   |
| [Compass](http://compass-style.org) | ~1.0.0   |
| [Node.js](http://nodejs.org)        | ~0.10.0  |
| [Grunt.js](http://gruntjs.com)      | ~0.4.5   |

## Grunt plugins

Use the ``npm`` command to download the Grunt dependencies : ``npm install``.

| Plugins                                                                   | Versions |
| ------------------------------------------------------------------------- | -------- |
| [glob](https://github.com/isaacs/node-glob)                               | ~4.0.5   |
| [load-grunt-tasks](https://github.com/sindresorhus/load-grunt-tasks)      | ~0.6.0   |
| [grunt-contrib-concat]													| ~0.4.0   |
| [grunt-contrib-clean]														| ~0.4.0   |
| [grunt-contrib-uglify]													| ~0.5.1   |
| [grunt-contrib-cssmin]													| ~0.10.0  |
| [grunt-contrib-copy]														| ~0.5.0   |
| [grunt-modernizr]															| ^0.5.2   |
| [grunt-webfont]                                                           |  0.4.2   |
| [time-grunt]																| ^0.4.0   |
| [matchdep]																| ~0.3.0   |
| [grunt-contrib-compass](https://github.com/gruntjs/grunt-contrib-compass) | ~0.9.1   |
| [grunt-contrib-watch](https://github.com/gruntjs/grunt-contrib-watch)     | ~0.6.1   |


## Bower packages

Use the ``bower`` command to download the Bower dependencies : ``bower install``.

| Libraries                                                     | Versions |
| ------------------------------------------------------------- | -------- |
| [jquery](http://jquery.com)                                   | ~1.11.1  |
| [jquery.notready](http://www.johansatge.fr/jquery-notready)   | latest   |
| [modernizr](http://modernizr.com)                             | ~2.8.3   |
| [sass-mercury](https://github.com/agenceepsilon/sass-mercury) | ~1.1.0   |

## Installation and running

* Run **'bower install'** to install all the dependencies
* Run **'npm install'** to install all the required node/grunt modules

Then when you're working on your project, just run the following command:

```bash
grunt
```

make your assets available from the root with a symbolic link.
```bash
ln -s release/assets assets
```

## Grunt tasks
* **compile**: generate all css. Minified css and js. Concat js files and copies everything to the release directory
* **default**: does compile and watch on all files
* **validate**: --TO DO-- does compile and runs jslint and csslint
* **package**: --TO DO-- does a bower install first then compiles and compresses everything into a war and uploads to nexus