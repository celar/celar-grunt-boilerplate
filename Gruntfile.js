// bronnen:
// https://github.com/agenceepsilon/meduseld-boilerplate


module.exports = function(grunt){
    
    // Load grunt tasks
    require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

    // Register grunt tasks
    grunt.loadTasks('./grunt-tasks');

    var config = {
        pkg: grunt.file.readJSON('package.json'),
        env: process.env
    };

    // Displays the elapsed execution time of grunt tasks
    require('time-grunt')(grunt);

    grunt.util._.extend(config, loadConfig('./grunt-tasks/options/'));
    grunt.initConfig(config);

    //grunt.loadNpmTasks('grunt-contrib-watch');

    function loadConfig(path){
        var glob = require('glob');
        var object = {};
        var key;

        glob.sync('*', {cwd: path}).forEach(function(option){
            key = option.replace(/\.js$/, '');
            object[key] = require(path + option);
        });

        return object;
    }
};