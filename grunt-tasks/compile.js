module.exports = function (grunt) {
    grunt.registerTask('compile', "Build all components and copy all deliverables to the target folders", [
        'clean',
        'webfont',
        'modernizr',
        'copy',
        'copy:bowerscripts',
        'copy:scripts',
        'uglify',
        'compass',
        'cssmin',
        'copy:fonts',
        'copy:styles'//,
        //'concat'
    ]);
};
