module.exports = {
    main: {
        cwd: 'img/',
        src: '*',
        dest: 'release/assets/img/',
        flatten: true,
        filter: 'isFile',
        expand: true
    },
    fonts: {
        cwd: 'generated_sources/fonts/',
        src: ['**'],
        dest: 'release/assets/fonts/',
        expand: true
    },
    styles: {
        cwd: 'generated_sources/css/',
        src: ['*.min.css'],
        dest: 'release/assets/css/',
        expand: true
    },
    bowerscripts: {
        src: [
            'bower_components/jquery/dist/jquery.js'
            ],
        dest: 'release/assets/js/vendor/',
        expand: true,
        flatten: true
    },
    scripts: {
        files: [
            {
                cwd: 'generated_sources/js',
                src: '*.js',
                dest: 'release/assets/js/lib/',
                expand: true
            }
        ]
    }
};