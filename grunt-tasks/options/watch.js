module.exports = {
    options: {
        livereload: false
    },
    grunt: {
        files: ['Gruntfile.js']
    },
    css: {
        files: '/scss/**/*.scss',
        tasks: ['compass:theme', 'cssmin', 'copy:styles']
    }
};