//
//	Start a local server on port 8888
//
//	use:
//		http://localhost:8888
//

module.exports = {
    server: {
        options: {
            hostname: '*',
            port: 8888
        }
    }
};
