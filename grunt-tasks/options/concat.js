module.exports = {
    options: {
        separator: ';'
    },
    main: {
        src: [
            'bower_components/jquery/dist/jquery.js',
            'assets/js/modernizr-custom.js'
        ],
        dest: 'assets/js/generated/generated.js'
    }
};
