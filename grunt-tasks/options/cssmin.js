module.exports = {
    main: {
        files: [{
            expand: true,
            cwd: 'generated_sources/css/',
            src: ['*.css', '!*.min.css'],
            dest: 'generated_sources/css/',
            ext: '.min.css'
        }]
    }
};
