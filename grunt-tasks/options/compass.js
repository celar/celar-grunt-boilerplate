module.exports = {
    options: {
        load: "bower_components/sass-list-maps",
        cssDir: "generated_sources/css",
        sassDir: "scss",
        imagesDir: "img",
        fontsPath: "fonts",
        javascriptsDir: "js",
        assetCacheBuster: false,
        outputStyle: "expanded",
        relativeAssets: true,
        noLineComments: true,
        force: true
    },
    theme: {
        /**
         * The "sourcemap" option replaces the "debug", it is only compatible with Chrome and "dev tool" native Firefox with Firebug not yet.
         * Choose the "debug" if you're running Firefox with Firebug and FireSass, a mandatory option.
         */
        options: {
            //debug: false,
            sourcemap: false
        }
    }
};
