module.exports = {
    main: {
        files: [{
            src: 'js/**/*.js',  // source files mask
            dest: 'release/assets',    // destination folder
            expand: true,    // allow dynamic building
            flatten: false,   // remove all unnecessary nesting
            ext: '.min.js'   // replace .js to .min.js
        },
            {
                src: 'generated_sources/js/lib/*.js',  // source files mask
                dest: 'release/assets/js/lib/',    // destination folder
                expand: true,    // allow dynamic building
                flatten: true,   // remove all unnecessary nesting
                ext: '.min.js'   // replace .js to .min.js
            },
            {
                src: 'release/assets/js/vendor/*.js',  // source files mask
                dest: 'release/assets/js/vendor/',    // destination folder
                expand: true,    // allow dynamic building
                flatten: true,   // remove all unnecessary nesting
                ext: '.min.js'   // replace .js to .min.js
            }
        ]
    }
};
