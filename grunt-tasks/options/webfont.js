module.exports = {
    delivery: {
        src: 'img/icons/*.svg',
        dest: 'generated_sources/fonts',
        destCss: 'generated_sources/scss',
        options: {
            font: 'icons',
            stylesheet: 'scss',
            templateOptions: {
                baseClass: 'glyph-icon',
                classPrefix: 'glyph_',
                mixinPrefix: 'glyph-'
            },
            relativeFontPath: '/assets/fonts',
            engine: 'node'
        }
    }
};
