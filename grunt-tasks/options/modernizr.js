module.exports = {
    main: {
        // [REQUIRED] Path to the build you're using for development.
        // devFile : "node_modules/grunt-modernizr/lib/modernizr-dev.js",
        devFile : "bower_components/modernizr/feature-detects/",
        // [REQUIRED] Path to save out the built file.
        outputFile : "generated_sources/js/modernizr-custom.js",

        // Based on default settings on http://modernizr.com/download/
        extra : {
            shiv : true,
            printshiv : false,
            load : true,
            mq : true,
            cssclasses : true
        },

        // Based on default settings on http://modernizr.com/download/
        extensibility : {
            addtest : false,
            prefixed : false,
            teststyles : false,
            testprops : false,
            testallprops : false,
            hasevents : false,
            prefixes : false,
            domprefixes : false
        },

        // By default, source is uglified before saving
        uglify : false,

        // Define any tests you want to implicitly include.
        tests : ["fontface", "backgroundsize", "borderradius", "boxshadow", "flexbox", "flexboxlegacy", "opacity", "rgba", "cssanimations", "csstransitions", "svg", "touch", "svgclippaths", "csstransitions"],

        // By default, this task will crawl your project for references to Modernizr tests.
        // Set to false to disable.
        parseFiles : false,

        // match user-contributed tests.
        matchCommunityTests : false,

        // Have custom Modernizr tests? Add paths to their location here.
        customTests : []
    }
};
